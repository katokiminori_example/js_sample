$(document).ready(function() {
    // getを使用してClick後にレスポンスを画面上に表示させるイベント
    $('#responseGetter').on('click', function() {
        $('#result').val('');
        let url = $('#url').val();
        get(url).then(function(response) {
            console.log(response);
            // 取得したresponseの文字列で書き換えられる
            $('#result').text(JSON.stringify(response));
            $('#url').val('');
        }, function(error) {
            console.log('error!', error);
            $('#result').text(JSON.stringify(error));
        });
        // responseより先に出力されるConsoleログ
        console.log("getting......");
        // responseにより値が書き換えられるまでの間に表示される文字列
        $('#result').text('レスポンス取得中...');
    });
    // getを使用してClick後にレスポンスを画面上に表示させるイベント
    $('#responseGetterAsync').on('click', function() {
        $('#result').val('');
        let url = $('#url').val();
        getAsync(url).then(function(response) {
            console.log(response);
            // 取得したresponseの文字列で書き換えられる
            $('#result').text(JSON.stringify(response));
            $('#url').val('');
        }, function(error) {
            console.log('error!', error);
            $('#result').text(JSON.stringify(error));
        });
        // responseより先に出力されるConsoleログ
        console.log("getting......");
        // responseにより値が書き換えられるまでの間に表示される文字列
        $('#result').text('レスポンス取得中...');
    });

});
// Promiseオブジェクトを戻すHTTP：Getのfunciton
function get(url) {
    return new Promise(function(resolve, reject) {
        $.ajax({
            type: 'get',
            url: url
        }).then(
            function(data) {
                resolve(data);
            },
            function(error) {
                reject(error);
            });
    });
};
//Promiseオブジェクトを戻すHTTP：Getのfunction(Async使用)
async function getAsync(url) {
    let result;
    // awaitによって処理終了まで待つ
    await $.ajax({
        type: 'get',
        url: url
    }).then(
        function(data) {
            result = data;
        },
        function(error) {
            result = error;
        });
    return result;
}