Rails.application.routes.draw do
  resources :users
  post 'users/ajaxCreate', 'users#ajaxCreate'
  get 'samples/jsSample', 'samples#jsSample'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
